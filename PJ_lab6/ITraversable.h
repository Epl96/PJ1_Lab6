#pragma once
#include <functional>

template <class T>
class ITraversable 
{
public:
	virtual void traverse(std::function<void(T)>) const = 0;
	virtual T& operator[](unsigned) = 0;
};