#pragma once
#include <vector>
#include <functional>
#include <iostream>
#include "ITraversable.h"

template <class T>
class FilteredBufferedArray : public ITraversable<T>
{
	template <class T>
	friend std::ostream& operator<<(std::ostream &str, const FilteredBufferedArray<T> &t) noexcept;

public:

	FilteredBufferedArray(std::function<bool(T)>) noexcept;
	void add(const T&) noexcept(false);
	void traverse(std::function<void(T)>) const;
	T& operator[](unsigned) override;

private:
	std::vector<T> arr;
	std::function<bool(T)> condition;
};

template <class T>
FilteredBufferedArray<T>::FilteredBufferedArray(std::function<bool(T)> condition)noexcept : condition(condition) {}

template<class T>
void FilteredBufferedArray<T>::add(const T &element) noexcept(false)
{
	if (condition(element)) arr.push_back(element);
}

template<class T>
void FilteredBufferedArray<T>::traverse(std::function<void(T)> f) const
{
	for (auto it : arr) { f(it); }
}

template<class T>
T& FilteredBufferedArray<T>::operator[](unsigned i)
{
	try
	{
		if (i > arr.size() - 1) { throw std::out_of_range("Out of range."); }
	}
	catch (std::exception &exc) {/*TODO: do some exception handling stuff*/}

	return arr[i];
}

template <class T>
std::ostream& operator<<(std::ostream &str, const FilteredBufferedArray<T> &t) noexcept
{
	for (auto &i : t.arr) { str << i << ' '; }
	return str << std::endl;
}