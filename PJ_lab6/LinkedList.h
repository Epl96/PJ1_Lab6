#pragma once
#include <list>
#include <iterator>
#include "ITraversable.h"
#include <exception>
#include <stdexcept>

template <class T>
class LinkedList : public ITraversable<T>
{
public:
	void add(const T&);
	void traverse(std::function<void(T)>) const override;
	T& operator[](unsigned) override;
private:
	std::list<T> list;
};

template <class T>
T& LinkedList<T>::operator[](unsigned i) 
{
	auto iterator = list.begin();
	try 
	{
		if (i > ((signed)list.size() - 1)) { throw std::out_of_range("Out of range!"); }
		std::advance(iterator, i);
	}
	catch (std::exception &exc) {/*TODO: do some exception handling stuff*/};

	return *iterator;
}

template<class T>
void LinkedList<T>::add(const T &element) 
{
	list.push_back(element);
}

template <class T>
void LinkedList<T>::traverse(std::function<void(T)> f) const 
{
	for (auto& t : list) { f(t); }
}

