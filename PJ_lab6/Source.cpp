#include "FilteredBufferedArray.h"
#include "LinkedList.h"

int main()
{
	LinkedList<int> list;
	list.add(5);
	list.add(3);
	list.add(-1);
	list.traverse([](int a) {std::cout << a << ' '; });

	FilteredBufferedArray<int> arr([](int a) {return a > 0; });


	arr.add(5);
	arr.add(3);
	arr.add(-3);

	arr.traverse([](int a) {std::cout << a << ' '; });

	system("pause");
}